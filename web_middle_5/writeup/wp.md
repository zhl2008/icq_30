### 1.题目名称

Hello Admin!



### 2.题目类型

web(middle)



### 3.题目描述

在一次网络扫描中扫到了这样一个站，你能拿到他的shell吗。



### 4.考察知识点

考察基本的x-forwarded-for，cookie伪造和命令执行



### 5.解题思路

访问一下发现提示IP forbidden! Only local!

尝试利用x-forwarded-for伪造ip为localhost，即127.0.0.1，访问成功

提示为访客身份登陆，查看cookie，发现为一串md5，解码结果为guest

尝试将cookie设为admin的md5值，发现以admin身份成功登陆

之后能够使用一个计算器功能，该功能为eval执行，尝试使用命令执行

通过system('ls')能够列举目录，看到flag.php，则system('cat flag.php')即可得到flag







