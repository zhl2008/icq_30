

# ICQ-30-WEB-INSPELLIAM-2



文档变更记录

（A：增加、M：修改、D：删除）

| 版本 | 状态 | 参与者     | 日期       | 描述 |
| ---- | ---- | ---------- | ---------- | ---- |
| V1.0 | A    | INSPELLIAM | 2018.09.23 | 初稿 |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |



### 设计思路

php弱类型比较+php session序列化引擎导致的序列化问题



### 分值估计

中等



### 题目部署

1.使用dockerfile编译一个基本docker，命名为icq:14.04

2.使用docker.sh运行该docker



### flag设置

flag在flag.php中

> flag{7bd5b2be500897c9950490623f3fd25c0c658b1b}