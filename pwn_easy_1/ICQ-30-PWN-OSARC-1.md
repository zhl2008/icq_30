

# ICQ-30-PWN-OSARC-1





文档变更记录

（A：增加、M：修改、D：删除）

| 版本 | 状态 | 参与者 | 日期       | 描述 |
| ---- | ---- | ------ | ---------- | ---- |
| V1.0 | A    | OSARC  | 2018.09.24 | 初稿 |
|      |      |        |            |      |
|      |      |        |            |      |
|      |      |        |            |      |
|      |      |        |            |      |
|      |      |        |            |      |
|      |      |        |            |      |



### 设计思路

考察基本的栈溢出和ret2shellcode



### 分值估计

简单



### 题目部署

将附件下发给选手

将deployment/source目录下的pwn文件赋予执行权限：chmod +x pwn

修改deployment/docker/docker-compose.yml中的60001为想要部署的端口

执行

docker-compose up -d

将ip和端口作为题目描述下发给选手即可



### flag设置

flag为

flag{2c021cee56c6c9110a7ee957626f611e}