#!/usr/bin/env python

# visible ascii 32-126
res = {}

for i in range(32,127):
    res[chr(i)] = 'None'

for i in range(32,127):
    for j in range(32,127):
        k = i ^ j
        if k>=32 and k<=126:
            res[chr(k)] = '("%s"^"%s")' %(chr(i),chr(j))

wanted_string = 'ZXZhbCgkX0NPT0tJRVsnd2hlcmVfdGhlX2Z1Y2tfaV9hbSddKQ=='

final = ''
for c in wanted_string:
    final += res[c] + '.'

print final[:-1]
