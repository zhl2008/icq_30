CREATE DATABASE /*!32312 IF NOT EXISTS*/ `osarc` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `osarc`;

--
-- Table structure for table `secrets`
--
DROP TABLE IF EXISTS `admin`;
create  table `admin` (
        `id` int(10) not null primary key auto_increment,
        `username` varchar(20) not null ,
        `password` varchar(40) not null
     );

LOCK TABLES `admin` WRITE;
INSERT INTO `admin` VALUES (0,'admin','flag{36dd8730c87fb1c49fd1ea24b2b9343f}');
UNLOCK TABLES;
