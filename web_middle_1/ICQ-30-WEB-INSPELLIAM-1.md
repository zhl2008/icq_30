

# ICQ-30-WEB-INSPELLIAM-1



文档变更记录

（A：增加、M：修改、D：删除）

| 版本 | 状态 | 参与者     | 日期       | 描述 |
| ---- | ---- | ---------- | ---------- | ---- |
| V1.0 | A    | INSPELLIAM | 2018.09.21 | 初稿 |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |



### 设计思路

考察php伪协议+php弱类型比较+rce



### 分值估计

中等



### 题目部署

1.使用dockerfile编译一个基本docker（基本上所有的web题都会基于这个docker)

2.使用docker.sh运行该docker



### flag设置

flag在flag81af9c42cfd0f6b7286267bcfa08dfe7.php中

> flag{d4b36a3ce1dc53c7345e473d2460849a25408ec60c312247316318578a1b4ace}