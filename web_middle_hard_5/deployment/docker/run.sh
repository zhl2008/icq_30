#!/bin/bash

chmod -R 755 /var/www/html/ 
chmod -R 777 /var/www/html/app2/upload/ 
chmod -R 755 /var/www/html/app2/upload/index.html

chmod 444 /var/www/html/app2/flag_zaqwsxcde234I.txt
chown root:root /var/www/html/app2/flag_zaqwsxcde234I.txt

supervisord -n
