

# ICQ-30-WEB-HAOZIGEGE-5





文档变更记录

（A：增加、M：修改、D：删除）

| 版本   | 状态   | 参与者       | 日期         | 描述   |
| ---- | ---- | --------- | ---------- | ---- |
| V1.0 | A    | HAOZIGEGE | 2018.09.23 | 初稿   |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |



### 设计思路

考察基本的渗透技巧，对javascript的逆向，对平行越权的理解，以及对于hash长度扩展攻击的认知。



### 分值估计

very hard



### 题目部署

1.使用dockerfile编译一个基本docker（基本上所有的web题都会基于这个docker)

2.使用docker.sh运行该docker



### flag设置

flag在sqlite的数据库中

> flag{5b1141eac71af46ad3cf73a1a3bedb5a}