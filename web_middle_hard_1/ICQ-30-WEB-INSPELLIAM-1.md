

# ICQ-30-WEB-INSPELLIAM-4



文档变更记录

（A：增加、M：修改、D：删除）

| 版本 | 状态 | 参与者     | 日期       | 描述 |
| ---- | ---- | ---------- | ---------- | ---- |
| V1.0 | A    | INSPELLIAM | 2018.09.22 | 初稿 |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |



### 设计思路

Vim.swp+hash扩展长度攻击+phtml解析



### 分值估计

中等偏难



### 题目部署

1.使用dockerfile编译一个基本docker，命名为web:14.04

2.使用docker.sh运行该docker



### flag设置

flag在flag-86cd7dcaf7ed5936d3a8d88559ce961e.php文件中

> flag{68cfdb081057fa6d1d540319e311fa7a65c8ff13}