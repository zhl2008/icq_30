#!/bin/bash

sleep 1
service apache2 start
chmod 755 /var/www/html/cache
chown root:root /var/www/html/*.php
chown root:root /var/www/html/*.swp
/bin/bash
