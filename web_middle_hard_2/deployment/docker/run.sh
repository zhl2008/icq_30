#!/bin/bash
chown www-data:www-data /var/www/html -R

# initialize database
mysqld_safe --skip-grant-tables&
sleep 5
## change root password
mysql -uroot -e "use mysql;UPDATE user SET password=PASSWORD('icq!@#qwe') WHERE user='root';FLUSH PRIVILEGES;"
## restart mysql
service mysql restart
## execute sql file
mysql -uroot -picq\!\@\#qwe < /root/a.sql

chmod 444 /var/www/html/flag_0ba7bc92fcd57e337ebb9e74308c811f
chown root:root /var/www/html/flag_0ba7bc92fcd57e337ebb9e74308c811f

source /etc/apache2/envvars
tail -F /var/log/apache2/* &
exec apache2 -D FOREGROUND
service apache2 start
