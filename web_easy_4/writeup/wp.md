### 1.题目名称

Bitcoin private-key system



### 2.题目类型

web(easy)



### 3.题目描述

暗网刚刚启用了一个比特币私钥存储系统，尝试入侵并获取管理员的私钥。



### 4.考察知识点

sqli



### 5.解题思路

通过简单的测试后，猜测post的session_id字段存在sql注入，可以使用sqlmap注入得到结果：

> sqlmap -u "http://127.0.0.1/index.php" --data "session_id=1" —dump







