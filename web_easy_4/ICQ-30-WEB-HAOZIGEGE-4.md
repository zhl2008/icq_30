

# ICQ-30-WEB-HAOZIGEGE-4





文档变更记录

（A：增加、M：修改、D：删除）

| 版本   | 状态   | 参与者       | 日期         | 描述   |
| ---- | ---- | --------- | ---------- | ---- |
| V1.0 | A    | HAOZIGEGE | 2018.09.22 | 初稿   |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |



### 设计思路

考察基本的sql注入漏洞查找，以及sqlmap的基本使用



### 分值估计

简单



### 题目部署

1.使用dockerfile编译一个基本docker（基本上所有的web题都会基于这个docker)

2.使用docker.sh运行该docker



### flag设置

flag在数据库中

> 'flag{simple_simple_simple_sql_injection}