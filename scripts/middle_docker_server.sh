#!/bin/bash
docker rm -f $(docker ps -aq)
# ports from 8005 to 8010
cd /home/icq_30/web_middle_1/deployment/docker; ./docker.sh
cd /home/icq_30/web_middle_2/deployment/docker; ./docker.sh
cd /home/icq_30/web_middle_3/deployment/docker; ./docker.sh
cd /home/icq_30/web_middle_4/deployment/docker; ./docker.sh
cd /home/icq_30/web_middle_5/deployment/docker; ./docker.sh
cd /home/icq_30/web_middle_6/deployment/docker; ./docker.sh
