

# ICQ-30-WEB-INSPELLIAM-3



文档变更记录

（A：增加、M：修改、D：删除）

| 版本 | 状态 | 参与者     | 日期       | 描述 |
| ---- | ---- | ---------- | ---------- | ---- |
| V1.0 | A    | INSPELLIAM | 2018.09.24 | 初稿 |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |
|      |      |            |            |      |



### 设计思路

考察弱口令+sql手注



### 分值估计

中等



### 题目部署

1.使用dockerfile编译一个基本docker（基本上所有的web题都会基于这个docker)

2.使用docker.sh运行该docker



### flag设置

flag放在flag_is_that_easy_to_find表中

> flag{36dd8730c87fb1c49fd1ea24b2b9343f}