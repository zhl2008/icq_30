CREATE DATABASE /*!32312 IF NOT EXISTS*/ `sqli` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sqli`;

--
-- Table structure for table `secrets`
--
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `blogs`;
DROP TABLE IF EXISTS `flag_is_that_easy_to_find`;
create  table `users` (
        `id` int(10) not null primary key auto_increment,
        `username` varchar(20) not null ,
        `password` varchar(40) not null
     );

create  table `blogs` (
        `id` int(10) not null primary key auto_increment,
        `content` varchar(1024) not null
     );

create  table `flag_is_that_easy_to_find` (
        `id` int(10) not null primary key auto_increment,
        `content` varchar(1024) not null
     );

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (0,'admin','qwerty');
UNLOCK TABLES;


LOCK TABLES `blogs` WRITE;
INSERT INTO `blogs` VALUES (1,'Do you want to get flag?');
INSERT INTO `blogs` VALUES (2,'Flag is near ...');
INSERT INTO `blogs` VALUES (3,'.....');
INSERT INTO `blogs` VALUES (4,'There is nothing I can help you!');
UNLOCK TABLES;


LOCK TABLES `flag_is_that_easy_to_find` WRITE;
INSERT INTO `flag_is_that_easy_to_find` VALUES (1, 'flag{36dd8730c87fb1c49fd1ea24b2b9343f}');
UNLOCK TABLES;
