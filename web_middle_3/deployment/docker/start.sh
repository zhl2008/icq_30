#!/bin/bash

sleep 1

/etc/init.d/apache2 start
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
service mysql start

chmod 777 /var/www/html
mysql -uroot < /root/setup.sql
/bin/bash
