

# ICQ-30-WEB-HAOZIGEGE-6





文档变更记录

（A：增加、M：修改、D：删除）

| 版本   | 状态   | 参与者       | 日期         | 描述   |
| ---- | ---- | --------- | ---------- | ---- |
| V1.0 | A    | HAOZIGEGE | 2018.09.23 | 初稿   |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |
|      |      |           |            |      |



### 设计思路

考察渗透技巧，php特性理解，反序列化漏洞的利用，以及对于常见的http跳转与协议的理解和应用。



### 分值估计

very hard



### 题目部署

1.使用dockerfile编译一个基本docker（基本上所有的web题都会基于这个docker)

2.使用docker.sh运行该docker



### flag设置

flag在flag_u_c4n_n07_guess.php中

> flag{CYP_g0_70_d13}