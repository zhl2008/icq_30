<?php
    include_once("common.php");
    if(!isset($_SESSION['username'])){
        header("Location: redirect.php?redirect=index.php?act=login");
        exit();
    }
    $user = base64_decode($_SESSION['username']);
?>
<html>
<head>
<meta charset="utf-8">
<title>USER</title>
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-theme.min.css" />
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    $(function(){
        $("#upload").click(function(){
            $("#pic").click();
        })
        $("#pic").on("change", function(){
            $("#upload_form").submit();
        })
    })
</script>
</head>
<body>
   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Mother ship</a></li>
            <li><a href="index.php?act=login">Login</a></li>
            <li><a href="index.php?act=user">User</a></li>
          </ul>
         <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php?act=user"><?php echo "welcome, ".base64_decode($_SESSION['username'])." skywalker";?></a></li>
         </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    


<div class="container">
	<br>
	<br>
	<br>
        <h2>Weapons/armies of <?php 
            echo $user;
        ?> </h2>
        <img class="img img-circle img-responsive" src="upload/weapon1.png" />
        <img class="img img-circle img-responsive" src="upload/weapon2.png" />
        <?php
            foreach ($piclist as $img) {
                echo '<img class="img img-circle img-responsive" src="' . $img .'" />';
            }
        ?>
        <div class="col-md-4 col-md-4-offset"></div>
        <form class="form-groups" id="upload_form" method="post" action="index.php?act=upload" enctype="multipart/form-data">
        <input type="file" id="pic" name="pic" style="display: none; " />
        <?php
            if($_SESSION['token']===1){
                echo '<div class="col-md-4 col-md-4-offset"><img class="img img-rounded" id="upload" style="border: 2px dashed black; cursor: pointer" src="upload/add.png" id="upload" /></div>';
            }else{
                echo '<div style="display: block; text-align: center; clear: both;"><h2>you know<br/><h6 style="color: grey">you are not our master</h6></h2>';
            }
        ?>
    </form>
</div>
</div>
<br><br><br>
<div class="col-md-4 col-md-offset-4">
<form class="form-signin" method=POST action=index.php?act=remote_upload>
        <h2 class="form-signin-heading">Input the beacon for reinforce</h2>
        <label for="Photo url" class="sr-only"></label>
        <input type="url" id="URL" name="url" class="form-control" placeholder="Photo url" required>
	<input type="hidden" id="debug" name="debug" value="0" class="form-control">
        <button class="btn btn-lg btn-primary btn-block" type="submit">GET IT</button>
</form>
</div>


</body>
</html>
