<?php
include_once("common.php");
if(isset($_GET["act"]) && preg_match('/^[a-z0-9_]+$/is', $_GET["act"])) {
    include_once __DIR__ . "/" . $_GET["act"] . ".php";
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Star Wars</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/starter-template.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Mother ship</a></li>
            <li><a href="index.php?act=login">Login</a></li>
            <li><a href="index.php?act=user">User</a></li>
          </ul>
	 <ul class="nav navbar-nav navbar-right">
	    <li style="float:right"><a href=index.php?act=user><?php echo "welcome, ".base64_decode($_SESSION['username'])." skywalker";?></a></li>
	 </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <div class="starter-template">
        <h1>Welcome to the battlefront!</h1>
        <p class="lead">Please Hack the death star!</p>
	<img src="upload/death_star.png">
      </div>

    </div><!-- /.container -->


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

