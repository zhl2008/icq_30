<?php
    if(isset($_REQUEST["username"]) && isset($_REQUEST["password"])){
        session_start();
        $ip = $_SERVER["REMOTE_ADDR"];
        if($ip == "::1" || $ip == "127.0.0.1"){
            $_SESSION["token"] = 1;    
	    $_SESSION['username'] = base64_encode('haozi');
        }else{
            $_SESSION['username'] = base64_encode($_POST["username"]);
            $_SESSION["token"] = 0;
        }
            header("Location: redirect.php?redirect=index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Signin Template for Bootstrap</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">

      <form class="form-signin" method=POST action=index.php?act=login>
        <h2 class="form-signin-heading">Death star security check</h2>
        <label for="Username" class="sr-only">Username</label>
        <input type="username" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->


  </body>
</html>
