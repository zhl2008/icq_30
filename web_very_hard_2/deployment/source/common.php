<?php
if(isset($_GET["include"])){
    set_include_path(get_include_path() . PATH_SEPARATOR . $_GET["include"]);
}
//ini_set("display_errors", "Off");
spl_autoload_register();
//error_reporting(0);
session_start();
$piclist = isset($_COOKIE["piclist"]) ? unserialize($_COOKIE["piclist"]) : [];
    
    
function randmd5(){
    $len = 16;
    $ret = "";  
    for ($i = 0; $i < $len; $i++)
    {  
        $ret .= chr(mt_rand(0, 255));
    }
    return md5($ret);
}

function curl($url,$debug,$count){
$count += 1;
$header = [];
$curlobj = curl_init();
curl_setopt($curlobj,CURLOPT_URL,$url); 
curl_setopt($curlobj, CURLOPT_RETURNTRANSFER, 1);//raw output
curl_setopt($curlobj, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curlobj, CURLOPT_HEADER, 1);
curl_setopt($curlobj, CURLOPT_POST, 0);
curl_setopt($curlobj, CURLOPT_MAXREDIRS, 1);
if($count<2){
    curl_setopt($curlobj, CURLOPT_FOLLOWLOCATION, 1);
}else{
    curl_setopt($curlobj, CURLOPT_FOLLOWLOCATION, 0);
}
$result=curl_exec($curlobj);
$headerSize=curl_getinfo($curlobj,CURLINFO_HEADER_SIZE);
$url2 = curl_getinfo($curlobj,CURLINFO_EFFECTIVE_URL);
$res_header=substr($result, 0, $headerSize);
$result =substr($result,$headerSize);
curl_close($curlobj);
if($url!=$url2 && $count<2){
    return curl($url2,$debug,$count);
}
if($debug){
    return $res_header . "\n" . $result;
}
return $result;
}

function is_image($url){
    $file_extension = substr($url,-4);
    $protocol = substr($url,0,7);
    if(($file_extension==".jpg" || $file_extension=='.png') && $protocol=="http://"){
        return True;
    }
    return False;
}

?>
