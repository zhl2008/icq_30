<?php
$filename = str_replace('../', '', $_GET['f']);
if(substr($filename, 0, 1) === '/') exit(0);
if(!file_exists($filename)){
    die("<script>alert('file $filename not exists!');</script>");
}
readfile($filename);
?>
