### 1.题目名称

Can you find my source code?



### 2.题目类型

web(middle)



### 3.题目描述

你能找到我的源码吗。



### 4.考察知识点

考察基本的vim备份文件修复，口令爆破和parse_str函数的利用



### 5.解题思路

简单扫一下目录可以发现备份文件index.php.bak

下载后发现是vim临时文件，恢复后得到源码，进行审计

首先发现需要爆破出密码，经过提示发现提供了字典pass_dict.txt

利用该字典进行爆破得到管理员密码登陆

登陆后发现需要$easy变量等于HelloWorld才能打印出flag

利用parse_str函数特性，构造形如input=easy=HelloWorld的payload得以通过检查

从而得到flag







